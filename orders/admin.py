from django.contrib import admin

# Register your models here.

# Register your models here.
from .models import *
# Register your models here.
admin.site.register(BillingAddress)
admin.site.register(ShippingAddress)
admin.site.register(Order)
admin.site.register(Basket)

