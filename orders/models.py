from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from mainWebsite.models import Product

# Create your models here.

class BillingAddress(models.Model):
	customer = models.ForeignKey(User, null=True)
	title = models.CharField(max_length=64, blank=True, choices=[('Mr', 'Mr'), ('Miss', 'Miss'), ('Mrs', 'Mrs'), ('Ms', 'Ms'), ('Dr', 'Dr')])
	fullname = models.CharField(max_length=255,blank=True)
	line1 = models.CharField(max_length=255, verbose_name='First line of address')
	line2 = models.CharField(max_length=255, verbose_name='Second line of address', blank=True)
	state = models.CharField(max_length=255, verbose_name='State/County', blank=True)
	postcode = models.CharField(max_length=64, verbose_name='Post/Zip-code', blank=True)
	default = models.BooleanField(default=True)
	def __unicode__(self):
		return self.title

class ShippingAddress(models.Model):
	customer = models.ForeignKey(User, null=True)
	title = models.CharField(max_length=64, blank=True, choices=[('Mr', 'Mr'), ('Miss', 'Miss'), ('Mrs', 'Mrs'), ('Ms', 'Ms'), ('Dr', 'Dr')])
	fullname = models.CharField(max_length=255,blank=True)
	line1 = models.CharField(max_length=255, verbose_name='First line of address')
	line2 = models.CharField(max_length=255, verbose_name='Second line of address', blank=True)
	state = models.CharField(max_length=255, verbose_name='State/County', blank=True)
	postcode = models.CharField(max_length=64, verbose_name='Post/Zip-code', blank=True)
	default = models.BooleanField(default=True)
        def __unicode__(self):
        	return self.title

class Basket(models.Model):
    user = models.OneToOneField(User,null=True)
    product = models.ManyToManyField(Product,blank=True)
    quantity = models.IntegerField(default=0)
    delievery_charge = models.CharField(max_length=128,default="0.00")
    def __unicode__(self):
    	return self.user.username
    def add(self):
    	# return int(self.product.price) + int(self.delievery_charge)
    	pass
class Order(models.Model):
	
	number = models.CharField(max_length=128,default="00000")
	productList = models.ManyToManyField(Product,blank=True)
	total_incl_tax = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Order total (inc. tax)',null=True)
	total_excl_tax = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Order total (excl. tax)',null=True)
	
	
	
	
	status = models.CharField(max_length=100, verbose_name='Status', blank=True,choices=[('delievered', 'delievered'), ('underProcess', 'underProcess'), ('shipped', 'shipped'), ('packed', 'packed')] )
	payment_status=models.CharField(max_length=100, verbose_name='Status', blank=True,choices=[('COD', 'COD'), ('Debit', 'Debit'),('Credit', 'Credit'),('Net', 'Net')] )
	date_placed=models.DateTimeField(auto_now_add=True)
	billingaddress = models.ForeignKey(BillingAddress,null=True)
	shippingaddress = models.ForeignKey(ShippingAddress,null=True)
	note = models.TextField(blank=True)




	