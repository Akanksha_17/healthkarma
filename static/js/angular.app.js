var app = angular.module('healthkarmaApp', ['ngCookies']);


app.config(function($interpolateProvider,$httpProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
	$httpProvider.defaults.withCredentials = true;



  });





var apiUrl = 'http://162.243.120.134:8080/'


/*var apiUrl = 'http://127.0.0.1:8000/'*/
app.controller("loginCntrl", ['$scope','$http', function($scope, $http){

	$scope.loginAlert1 = 0;

	console.log("logincnt");
		$scope.loginUser = function() {

			
			console.log("submit fuc");
		var req = {
			method: 'POST',
			url: apiUrl+"login/",
			header: {
				'Content-Type': "application/json"
			},
			data: $.param({
				username: $scope.usernameM,
				password: $scope.passwordM
			})
		};


		$http(req).then(function(result){
			console.log("got result");
			location.reload();
			

		}, function(){
			$scope.loginAlert1 = 1;
			console.log("error");

			

		});
	};

		



}]);



app.controller("productSearchCntrl", function($scope, $http, $cookies){

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

	console.log("contrller loaded");


	$scope.products = [];
	$scope.carditems = [0];
	$scope.addedToCart = false;







	$scope.checkIfaddedToCart = function(ppk) {
		angular.forEach($scope.carditems, function(val,key) {
			console.log($scope.carditems,val)

			if(val == ppk)
				return 0;

			else
				return 1;

		});
	}




	$scope.addtocard = function(productPk) {

		console.log("add to cart called");
		

		var req = {
			method: 'GET',
			url: apiUrl+'basket/'+productPk+'/',
			header: {
				'Content-Type': "application/json"
			}
		};


		$http(req).then(function(res) {
			$scope.carditems.push(productPk);
			$scope.addedToCart = true;
			console.log("added to card");

		});
	};



		$scope.deleteContentInCart = function(productPk) {

		console.log("add to cart called");
		

		var req = {
			method: 'DELETE',
			url: apiUrl+'basket/'+productPk+'/',
			header: {
				'Content-Type': "application/json"
			}
		};


		$http(req).then(function(res) {
			console.log("REMOVED FRM card");
			location.reload();

		});
	};



})