from django.contrib import admin

# Register your models here.
from .models import *
# Register your models here.

admin.site.register(TempUser)
admin.site.register(customerDetail)
admin.site.register(BookedEvent)
admin.site.register(BookedOrder)

# admin.site.register(ShippingAddress)

