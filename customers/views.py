from django.shortcuts import render
from django.shortcuts import render_to_response,HttpResponseRedirect,HttpResponse
# Create your views here.
from .models import *
from .forms import *
from django.core.context_processors import csrf
from django.contrib.auth import authenticate,logout
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate,logout
from django.template import RequestContext
from orders.models import *

def signup(request):
    c={}
    c.update(csrf(request))
    return render_to_response('customers/signup.html',c)

def registration(request):
    details={}
    response = {}
    authdata = SignupForm(request.POST or None)
    if request.method == 'POST':
        if authdata.is_valid():
            print 'inside post data'
            tempUser=authdata.save()
            print tempUser
            
            
            return render_to_response('customers/registration.html')
        else:
            c={'form': authdata}
            c.update(csrf(request))
            print 'data not valid'
            print c
            return render_to_response('customers/signup.html',c)
    else:
        authdata = SignupForm()
        c={'form': authdata}
        c.update(csrf(request))
        print 'data not valid'
        print c
        return render_to_response('customers/signup.html',c)

def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/customer/dashboard/',context_instance=RequestContext(request))
    else:
        c={}
        c.update(csrf(request))
        return render_to_response('customers/login.html',c)

def authuser(request):
    response ={}
    authdata = LoginForm(request.POST or None)
    if request.method == 'POST':
        if authdata.is_valid():
            print 'in views'
            user = User.objects.get(username=authdata.cleaned_data["username"])
            
            user = authenticate(username=user.username, password=authdata.cleaned_data["password"])
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    
                    return HttpResponseRedirect('/customer/dashboard/')
        else:
            c={'form': authdata}
            print "invalid data"
            print c
            c.update(csrf(request))
            return render_to_response('customers/login.html',c)            
    else:
        authdata = LoginForm()
        print "not post"
        print c
        c={'form': authdata}
        c.update(csrf(request))
        return render_to_response('customers/login.html',c)

def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/')


def confirm(request, confirmation_code, pk):
    details={}
    try:
        user = User.objects.get(pk=pk)
        profile = TempUser.objects.get(userobj=user)
        
        
        if profile.confirmation_code == confirmation_code:
            user.is_active = True
            user.save()
            profile.delete()
            
        return HttpResponseRedirect('/customer/login/')
    except:
        return HttpResponseRedirect('/')


def dashboard(request):
    context={}
    j=1
    print 'here'

    
    try:
        customer = customerDetail.objects.get(user=request.user)
        print customer.user.username
        context['products'] = customer.orders.all()
        context['events'] = customer.eventList.all()
        context['bookings'] = BookedOrder.objects.filter(user=request.user)
        
        

    except:
        context={}
        print 'there'
    return render_to_response('customers/dashboard.html',context,context_instance=RequestContext(request))





