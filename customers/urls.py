from django.conf.urls import patterns, url, include
import views
urlpatterns = patterns('',url(r'^signup/$',views.signup,name='signup'),
	url(r'^registration/$',views.registration,name='result'),
	url(r'^confirm/(?P<confirmation_code>[\w-]+)/(?P<pk>[\w-]+)/$',views.confirm,name='confirm'),
	url(r'^login/$',views.login,name='login'),
	url(r'^dashboard/$',views.dashboard,name='dashboard'),
	url(r'^login/success/$',views.authuser,name='success'),
	url(r'^logout/$',views.logout_user,name='logout'),
	)