from django import forms
from .models import *
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login,logout
import random
import string
from django.core.mail import send_mail
from healthkarma.settings import DEFAULT_FROM_EMAIL

def send_registration_confirmation(user):
    p = TempUser.objects.get(userobj=user)
    print 'inside the mail'
    title = "Health Karma Account confirmation"
    content = "http://162.243.120.134:8000/customer/confirm/" + str(p.confirmation_code) + "/" + str(user.pk) + "/"
    send_mail(title, content,DEFAULT_FROM_EMAIL, [user.username], fail_silently=False)

class SignupForm(forms.Form):
    error_messages = {
        'already_exist': ("Another account is already associated with this credentials"),}
    password = forms.CharField(label=("password"),widget=forms.PasswordInput)
    email = forms.EmailField(label=("email"),widget=forms.EmailInput)
    fname = forms.CharField(label=("firstname"),widget=forms.TextInput)
    lname = forms.CharField(label=("lastname"),widget=forms.TextInput)
    
    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        print '###########'
        print cleaned_data
        email = cleaned_data.get("email")
        fname=cleaned_data.get("fname")
        lname = cleaned_data.get("lname")
        password = cleaned_data.get("password")
        



        a = User.objects.filter(email=email)
        
        if a :
            raise forms.ValidationError(
                self.error_messages['already_exist'],
                code='already_exist',)

    def save(self, commit=True):
        confirmation_code = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for x in range(33))
        print 'confirmationCode' + str(confirmation_code)
        user,created = User.objects.get_or_create(username=self.cleaned_data['email'],email=self.cleaned_data['email'],first_name=self.cleaned_data['fname'],last_name=self.cleaned_data['lname'])
        user.set_password(self.cleaned_data['password'])
        user.is_active = False
        user.save()
        p = TempUser(userobj=user, confirmation_code=confirmation_code)
        p.save()
        print 'saving temp user'
        send_registration_confirmation(user)
        if commit:
            p.save()
        return p


class LoginForm(forms.Form):

    username  =  forms.CharField(required=False,label=("username"))
    
    password = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': ("Please enter a correct %(username)s and password. "
                           "Note that both fields may be case-sensitive."),
        'inactive': ("This account is inactive."),
        'does_not_exist' : ("User does not exist"),
    }



    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get('username')
        
        password = cleaned_data.get('password')
        try:
            user = User.objects.get(username=username)

        
            
        except:
            raise forms.ValidationError(
                    self.error_messages['does_not_exist'],
                    code='does_not_exist',
                    params={'username': "email"},

                )

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': "email"},

                )
            else:
                self.confirm_login_allowed(self.user_cache)



        return   cleaned_data


    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

