from django.db import models
from django.contrib.auth.models import User
import random
import urllib2,datetime,binascii,os
from mainWebsite.models import Event
from orders.models import Order



# Create your models here.
  



class TempUser(models.Model):
    """ This model saves those user profile account which are not activated
    """
    userobj = models.OneToOneField(User,null=True)
    confirmation_code = models.CharField(max_length=100,default='test')
    def __unicode__(self):
    	return self.userobj.username

class BookedEvent(models.Model):
	event = models.ForeignKey(Event,null=True)
	registrationId = models.CharField(max_length=100,default='000')
	payment_method=models.CharField(max_length=100, verbose_name='Status', blank=True,choices=[('Paytm', 'Paytm'), ('Debit', 'Debit'),('Credit', 'Credit'),('Net', 'Net')] )
	def __unicode__(self):
		return self.event.title
class BookedOrder(models.Model):
    user = models.OneToOneField(User,null=True)
    title =  models.CharField(max_length=100,default='test')
    price = models.CharField(max_length=128,default="111")
    date_created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to="mainwebsite/products/",default='group.jpg')
    status = models.CharField(max_length=100, verbose_name='Status', blank=True,choices=[('delievered', 'delievered'), ('underProcess', 'underProcess'), ('shipped', 'shipped'), ('packed', 'packed')] )
    def __unicode__(self):
        return self.title

class customerDetail(models.Model):
    user = models.OneToOneField(User,null=True)
    orders = models.ManyToManyField(Order,blank=True)
    eventList = models.ManyToManyField(BookedEvent,null=True)
    def __unicode__(self):
    	return self.user.username


  