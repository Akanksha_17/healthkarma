# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-08 13:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainWebsite', '0003_auto_20160908_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogimages',
            name='featureImage',
            field=models.ImageField(upload_to='mainwebsite/blogs/'),
        ),
        migrations.AlterField(
            model_name='event',
            name='featureImage',
            field=models.ImageField(upload_to='mainwebsite/events/'),
        ),
        migrations.AlterField(
            model_name='event',
            name='startDate',
            field=models.DateField(default=datetime.datetime(2016, 9, 8, 13, 26, 53, 169379)),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='image',
            field=models.ImageField(upload_to='mainwebsite/category/'),
        ),
    ]
