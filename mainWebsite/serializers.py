from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User
class UserSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model=User
        fields=('username')
class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model=Product
        fields=('pk','title','price','description','rating','quantity','image')

class CategorySerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = ProductCategory
		fields = ('pk','name','description')