from django.conf.urls import patterns, url, include
import views

urlpatterns = patterns('',url(r'^$',views.home,name='home'),
	url(r'^blogs/$',views.blog_list_page,name='bloglist'),
	url(r'^blogs/(?P<slug>[\w-]+)/$',views.blog_detail, name='blogdetail'),
	url(r'^events/$',views.event_list_page,name='eventlist'),
	url(r'^events/(?P<slug>[\w-]+)/$',views.event_detail, name='eventdetail'),
	url(r'^store/$',views.store, name='store'),
	url(r'^product/categories/(?P<slug>[\w-]+)/$',views.product_list, name='categoryProducts'),
	url(r'^product/(?P<slug>[\w-]+)/$',views.product_detail, name='productdetail'),
	url(r'^standard/$',views.standard_page, name='standardpage'),
	url(r'^checkout/$',views.checkout_page, name='checkout'),
	url(r'^categories/$',views.Categorylist.as_view()),
	url(r'^basket/(?P<pk>[\w-]+)/$',views.add_basket),
	url(r'^delivery/$',views.delivery_page),
	url(r'^login/$',views.Login.as_view()),
	
	url(r'^success/(?P<pk>[\w-]+)/$',views.success),
	url(r'^add/address/$',views.addAddress),
	url(r'^event/register/(?P<slug>[\w-]+)/$',views.eventpay),
	url(r'^event/success/(?P<slug>[\w-]+)/$',views.eventsuccess),
	
	
	)

	