from __future__ import unicode_literals

from django.db import models
import datetime
from datetime import datetime

from django.contrib.auth.models import User
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField
from decimal import Decimal
# Create your models here.


class Location(models.Model):
	city = models.CharField(max_length=100,default="sample")
	# name = models.CharField(max_length=100,default="sample")
	venue = models.CharField(max_length=100,default="sample")
	def __unicode__(self):
		return self.city
class Event(models.Model):
	
	slug = models.SlugField(max_length=40,blank=True)
	title = models.CharField(max_length=100,default="sample")
	shortDescription = models.CharField(max_length=255,default="sample")
	description = models.TextField(blank=True)
	datetime = models.DateTimeField(default=datetime.now(), blank=True)
	trainer = models.CharField(max_length=100,default="sample")
	price = models.CharField(max_length=100,default="00")
	location = models.ForeignKey(Location,null=True)
	linkLabel = models.CharField(max_length=300,blank=True)
	featureImage = models.ImageField(upload_to="mainwebsite/events/")
	participantsNo = models.IntegerField(default=0)
	
	def __unicode__(self):
		return self.title
	def save(self, *args, **kwargs):
		self.slug= slugify(self.title)
		super(Event, self).save(*args, **kwargs)
	



class Blog(models.Model):
	title = models.CharField(max_length=100,default="sample")
	slugfield = models.SlugField(max_length=40,blank=True)
	content = models.TextField(blank=True)
	author = models.CharField(max_length=100,default="sample")
	coverimg = models.ImageField(upload_to="mainwebsite/blogs/",default='group.jpg')
	

	def __unicode__(self):
		return self.title
	def save(self, *args, **kwargs):
		self.slugfield= slugify(self.title)
		super(Blog, self).save(*args, **kwargs)

	


	

class ProductCategory(models.Model):
	name = models.CharField(max_length=100,default="sample")
	description = models.TextField(blank=True)
	image = models.ImageField(upload_to="mainwebsite/category/")

	slug= models.SlugField(max_length=255,blank=True)

	def __unicode__(self):
		return self.name
	def save(self, *args, **kwargs):
		self.slug= slugify(self.name)
		super(ProductCategory, self).save(*args, **kwargs)

class Product(models.Model):
	category = models.ForeignKey(ProductCategory,null=True)
	title = models.CharField(max_length=100,default="sample")
	slug= models.SlugField(max_length=255,blank=True)
	price = models.CharField(max_length=128,default="111")
	description = models.TextField(blank=True)
	rating = models.FloatField(null=True)
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now=True)
	quantity = models.IntegerField(default=0)
	is_discountable = models.BooleanField(default=False,help_text='This flag indicates if this product can be used in an offer or not')
	is_featured = models.BooleanField(default=False,help_text='This flag indicates if this product is a featured product')
	image = models.ImageField(upload_to="mainwebsite/products/",default='group.jpg')
	def __unicode__(self):
		return self.title
	def save(self, *args, **kwargs):
		self.slug= slugify(self.title)
		super(Product, self).save(*args, **kwargs)

class Answer(models.Model):
	ans = models.TextField(blank=True)
	correct = models.BooleanField(default=False)
class Question(models.Model):
	statement = models.TextField(blank=True)
	marks = models.IntegerField(default=0)
	options = models.ManyToManyField(Answer,blank=True)
	









