from django.shortcuts import render
from django.shortcuts import render_to_response,HttpResponseRedirect,HttpResponse
from .models import *
from rest_framework.decorators import api_view
from .serializers import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.template import RequestContext
from orders.models import *
from django.views.decorators.csrf import csrf_exempt
from django.core.context_processors import csrf
from django.contrib.auth import login
from django.contrib.auth import authenticate
from rest_framework.renderers import JSONRenderer
from customers.models import *
from datetime import datetime



# Create your views here.


def uniqid():
    from time import time
    return hex(int(time()*10000000))[2:]
class JSONResponse(HttpResponse):

    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)
def home(request):
	context={}
	context['eventsList'] = Event.objects.all()
	context['upcomingEventsList'] = Event.objects.filter(datetime__gte=datetime.today())
	context['pastEventsList'] = Event.objects.filter(datetime__lte=datetime.today())
	context['blogList'] = Blog.objects.all()
	context['productCategories'] = ProductCategory.objects.all()
	return render_to_response('mainWebsite/pages/index.html',context,context_instance=RequestContext(request))

def blog_list_page(request):
	context={}
	context['blogList'] = Blog.objects.all()
	return render_to_response('mainWebsite/pages/blog.html',context,context_instance=RequestContext(request))
def blog_detail(request,slug):
	context={}
	context['singleBlog'] = Blog.objects.get(slugfield=slug)
	return render_to_response('mainWebsite/pages/blogpage.html',context,context_instance=RequestContext(request))
def event_list_page(request):
	context={}
	context['eventList'] = Event.objects.all()
	return render_to_response('mainWebsite/pages/event.html',context,context_instance=RequestContext(request))
def event_detail(request,slug):
	context={}
	context['singleEvent'] = Event.objects.get(slug=slug)
	return render_to_response('mainWebsite/pages/eventdetail.html',context,context_instance=RequestContext(request))
def store(request):
	context={}
	context['categoryList'] = ProductCategory.objects.all()
	context['productList'] = Product.objects.all()
	return render_to_response('mainWebsite/pages/product-search.html',context,context_instance=RequestContext(request))
def product_list(request,slug):
	context={}
	context['categoryList'] = ProductCategory.objects.all()
	category = ProductCategory.objects.get(slug=slug)
	context['productList'] = Product.objects.filter(category=category)
	return render_to_response('mainWebsite/pages/product-category.html',context,context_instance=RequestContext(request))
def product_detail(request,slug):
	context={}
	context['product'] = Product.objects.get(slug=slug)
	return render_to_response('mainWebsite/pages/product-single.html',context,context_instance=RequestContext(request))

def checkout_page(request):
	context={}
	# context['product'] = Product.objects.get(slug=slug)
	try:
		s = 0
		context['basket'] = basket=Basket.objects.get(user=request.user)
		context['product'] = productList = basket.product.all()
		for i in productList:
			print s
			s= float(s) + float(i.price)

		context['sum'] = s
		context['total'] = float(s) + float(basket.delievery_charge)
	
	except:
		pass
	return render_to_response('mainWebsite/pages/checkout.html',context,context_instance=RequestContext(request))
def standard_page(request):
	context={}

	
	return render_to_response('mainWebsite/pages/standardpage.html',context,context_instance=RequestContext(request))
def delivery_page(request):
	context={}
	context.update(csrf(request))

	try:
		context['address'] = ShippingAddress.objects.get(customer=request.user)
	except:
		pass
	return render_to_response('customers/deliveryprocess.html',context,context_instance=RequestContext(request))

# def delivery_page(request):
# 	context={}
# 	try:
# 		context['address'] = ShippingAddress.objects.get(customer=request.user)
# 	except:
# 		pass
# 	return render_to_response('customers/deliveryprocess.html',context,context_instance=RequestContext(request))



def success(request,pk):
	context={}
	j=0
	s=0

	context['orderId'] = orderId= uniqid()
	try:
		customer = customerDetail.objects.get(user=request.user)
	except:
		customer = customerDetail(user=request.user)
		customer.save()
	try:
		basket= Basket.objects.get(pk=pk)
		shippingaddress = ShippingAddress.objects.get(customer=request.user)
		if request.POST['COD']:
			status = 'COD'
		elif request.POST['Debit']:
			status = 'Debit'
		elif request.POST['Credit']:
			status = 'Credit'
		else:
			status = "Net"

		order = Order(number=str(orderId),shippingaddress=shippingaddress,payment_status=status,status='underProcess')
		order.save()
		for i in basket.product.all():
			booking = BookedOrder(user=request.user,price=i.price,title=i.title,image=i.image,status='underProcess')
			booking.save()
			s = float(i.price) + float(s)
			print i
			item = Product.objects.get(pk=i.pk)
			order.productList.add(item)
		total = float(s) + float(basket.delievery_charge)
		order.total_incl_tax = total
		order.save()
		customer.orders.add(order)
		basket.delete()

		return render_to_response('mainWebsite/pages/success.html',context,context_instance=RequestContext(request))
	except:
		return render_to_response('mainWebsite/pages/failure.html',context,context_instance=RequestContext(request))

	
		



	
	
	
class Categorylist(APIView):
    """
    List all categories, or create a new category.
    """
    def get(self, request, format=None):
        category = ProductCategory.objects.all()
        serializer = CategorySerializer(category, many=True)
        return Response(serializer.data)



@api_view(['GET','DELETE'])
def add_basket(request,pk):
    context={}
    # context.update(csrf(request))

    if request.method == "GET":
    	product = Product.objects.get(pk=pk)
    	try:
    		basket = Basket.objects.get(user=request.user)
    		basket.product.add(product)

    	except:

    		basket = Basket(user=request.user,quantity=1)
    		basket.save()
    		basket.product.add(product)
    	basket.save()
    	
    	
        context['success'] = 1
        return Response(context,status=status.HTTP_200_OK)
    elif request.method == 'DELETE':
    	product = Product.objects.get(pk=pk)
    	print 'inside delete'
    	try:
    		basket = Basket.objects.get(user=request.user)
    		basket.product.remove(product)
      		context['success'] = 1
      	except:
      		pass
        return Response(context,status=status.HTTP_204_NO_CONTENT)
    else:
    	context['success'] = 0
        return Response(context,status=status.HTTP_400_BAD_REQUEST)

class Login(APIView):

    def post(self, request, format=None):
        context = {}
        
        renderer_classes = (JSONRenderer, )
        serializer = UserSerializer(data=request.data)
        user = authenticate(username=request.data['username'], password=request.data['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                context['username'] = request.user.username
            	context['success'] = 1
            	print request.user
            return Response(context,status=status.HTTP_200_OK)
        else:
        	context['success'] = 0
        	return Response(context,status=status.HTTP_400_BAD_REQUEST)

def addAddress(request):
	c={}
	c.update(csrf(request))
	s=0
	try:

		c['basket'] = basket=Basket.objects.get(user=request.user)
		c['product'] =productList= basket.product.all()
		for i in productList:
			print s
			s= float(s) + float(i.price)

		c['sum'] = s
		print c['sum']
		c['total'] = float(s) + float(basket.delievery_charge)

	except:
		pass

	if request.method == "POST":
		try:
			shippingAddress=ShippingAddress.objects.get(customer=request.user)
		except:

			shippingAddress = ShippingAddress(customer=request.user,title=request.POST['title'],fullname=request.POST['name'],line1=request.POST['line1'],line2=request.POST['line2'],state=request.POST['state'],postcode=request.POST['pin'])
			shippingAddress.save()

	return render_to_response('mainWebsite/pages/paymentprocess.html',c,context_instance=RequestContext(request)) 


def eventpay(request,slug):
    context={}
    context.update(csrf(request))

    
    try:
        context['event'] = Event.objects.get(slug=slug)

    except:
        context={}
        
    return render_to_response('mainWebsite/pages/eventpay.html',context,context_instance=RequestContext(request))

def eventsuccess(request,slug):
	context={}
	j=0
	context['registrationId'] = registrationId= uniqid()
	print registrationId
	try:
		
		customer = customerDetail.objects.get(user=request.user)
	except:
		
		customer = customerDetail(user=request.user)
		customer.save()
	try:
		print 'try2'
		event= Event.objects.get(slug=slug)
		print event
		print 'this' + str(request.POST)

		if 'Paytm' in request.POST:
			status = 'Paytm'
			
		elif 'Debit' in request.POST:
			status = 'Debit'
			
		elif 'Credit' in request.POST:
			status = 'Credit'
			
		else:
			status = "Net"
			
		
		registerEvent = BookedEvent(event=event,registrationId=registrationId,payment_method=status)
		
		registerEvent.save()
		
		customer.eventList.add(registerEvent)
		

		

		return render_to_response('mainWebsite/pages/eventsuccess.html',context,context_instance=RequestContext(request))
	except:
		return render_to_response('mainWebsite/pages/failure.html',context,context_instance=RequestContext(request))

