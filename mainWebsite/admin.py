from django.contrib import admin
from .models import *
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
# Register your models here.
class EventAdminForm(forms.ModelForm):
    
    class Meta:
        model = Event
        exclude = ("linkLabel",)
        widgets = {
            'description': CKEditorUploadingWidget(),
        }
class EventAdmin(admin.ModelAdmin):
	form = EventAdminForm
	list_filter = ('location__city',)
class BlogAdminForm(forms.ModelForm):
    
    class Meta:
        model = Blog
        exclude = ("slug",)
        widgets = {
            'content': CKEditorUploadingWidget(),
        }	
class BlogAdmin(admin.ModelAdmin):
	form = BlogAdminForm
	
admin.site.register(Location)
admin.site.register(Event,EventAdmin)
admin.site.register(Blog,BlogAdmin)
admin.site.register(ProductCategory)
admin.site.register(Product)
admin.site.register(Answer)
admin.site.register(Question)


