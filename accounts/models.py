from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from mainWebsite.models import Product
# Create your models here.

# class CompanyData(models.Model):

from customers.models import *
 
class Sales(models.Model):
	user = models.OneToOneField(User,blank=True)
	product = models.ForeignKey(Product,null=True)
	status = models.CharField(max_length=100, verbose_name='Status', blank=True,choices=[('paid', 'paid'),('unpaid', 'unpaid')] )